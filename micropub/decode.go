package micropub

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/gorilla/schema"

	mf2 "gitlab.com/jramsay/micropub-now/microformat"
)

func Unmarshal(r *http.Request) (*Entry, error) {
	switch r.Header.Get("Content-Type") {
	case "application/json":
		return unmarshalJSON(r)
	case "application/x-www-form-urlencoded":
		return unmarshalForm(r)
	}
	return nil, fmt.Errorf("Unsupported Content-Type %s", r.Header.Get("Content-Type"))
}

// Micropub Post JSON format
// https://www.w3.org/TR/micropub/#json-syntax
type requestJSON struct {
	Type       mf2.String   `json:"type"`
	Properties requestProps `json:"properties"`
}

type requestProps struct {
	Name    mf2.String   `json:"name"`
	Content mf2.String   `json:"content"`
	Date    mf2.Time     `json:"date"`
	Photo   []mf2.String `json:"photo"`
}

func unmarshalJSON(r *http.Request) (*Entry, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	var req requestJSON
	if err := json.Unmarshal(body, &req); err != nil {
		return nil, err
	}

	// TODO: normalize req.Type = h-entry
	entry := Entry{
		Type:    string(req.Type),
		Content: string(req.Properties.Content),
		Date:    time.Time(req.Properties.Date),
		Name:    string(req.Properties.Name),
		// Photos:  []string(req.Properties.Photo),
	}

	if media, err := entry.FetchMedia(); err == nil {
		entry.Media = media
	} else {
		return nil, err
	}

	return &entry, nil
}

type requestForm struct {
	Type    string    `schema:"h"`
	Content string    `schema:"content"`
	Name    string    `schema:"name"`
	Date    time.Time `schema:"date"`
	Photo   []string  `schema:"photo[]"`
}

func unmarshalForm(r *http.Request) (*Entry, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}

	var decoder = schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)
	decoder.RegisterConverter(time.Time{}, timeConverter)

	req := &requestForm{}
	if err := decoder.Decode(req, r.Form); err != nil {
		return nil, err
	}

	// TODO: normalize req.Type = entry
	entry := Entry{
		Type:    req.Type,
		Content: req.Content,
		Date:    req.Date,
		Name:    req.Name,
		Photos:  req.Photo,
	}

	if media, err := entry.FetchMedia(); err == nil {
		entry.Media = media
	} else {
		return nil, err
	}

	return &entry, nil
}

func timeConverter(value string) reflect.Value {
	// Epoch timestamps
	if i, err := strconv.ParseInt(value, 10, 64); err == nil {
		return reflect.ValueOf(time.Unix(i, 0))
	}

	// ISO8601
	if v, err := time.Parse("2006-01-02T15:04:05-07:00", value); err == nil {
		return reflect.ValueOf(v)
	}

	return reflect.Value{}
}

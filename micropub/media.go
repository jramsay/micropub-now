package micropub

import (
	"bytes"
	"errors"
	"io"
	"net/http"
)

func (e *Entry) FetchMedia() ([]*MediaFile, error) {
	var files []*MediaFile
	for _, url := range e.mediaURLs() {
		f, err := getMediaFile(url)
		if err != nil {
			return nil, err
		}

		files = append(files, f)
	}

	return files, nil
}

func (e *Entry) mediaURLs() []string {
	return e.Photos
}

func getMediaFile(url string) (*MediaFile, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, errors.New(res.Status)
	}

	var data bytes.Buffer
	if _, err := io.Copy(&data, res.Body); err != nil {
		return nil, err
	}

	lastModified, err := http.ParseTime(res.Header.Get("Last-Modified"))
	if err != nil {
		return nil, err
	}

	media := MediaFile{
		URL:          url,
		Data:         data.Bytes(),
		LastModified: lastModified,
	}

	return &media, nil
}

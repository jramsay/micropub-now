package micropub

import (
	"bytes"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gosimple/slug"
	"github.com/mattn/godown"
	"gopkg.in/yaml.v3"
)

// Entry implements Micropub h-entry
// TODO: add support for additional entry fields
// TODO: this is a micropub "post" of microformat type "entry"
type Entry struct {
	Type    string    `schema:"type"`
	Content string    `schema:"content"`
	Name    string    `schema:"name"`
	Date    time.Time `schema:"date"`
	Photos  []string  `schema:"photo"`
	Media   []*MediaFile
}

type MediaFile struct {
	URL          string
	Data         []byte
	LastModified time.Time
}

// String returns string reprsentation of h-entry
func (post *Entry) String() string {
	frontmatter, err := post.generateFrontmatter()
	if err != nil {
		log.Println(err)
	}

	content, err := post.MarkdownContent()
	if err != nil {
		content = post.Content
	}

	return fmt.Sprintf("---\n\n%s\n---\n\n%s\n", frontmatter, content)
}

func (post *Entry) generateFrontmatter() (string, error) {
	photos := post.Photos

	fm := &struct {
		Title      string    `yaml:"title,omitempty"`
		Slug       string    `yaml:"slug,omitempty"`
		Date       time.Time `yaml:"date,omitempty"`
		Photos     []string  `yaml:"photos,omitempty"`
		Categories []string  `yaml:"categories,omitempty"`
	}{
		Slug:   post.Slug(),
		Date:   post.Date,
		Photos: photos,
	}

	if len(photos) > 0 {
		fm.Categories = append(fm.Categories, "Photos")
	}

	b, err := yaml.Marshal(fm)
	if err != nil {
		return "", fmt.Errorf("error generating frontmatter yaml")
	}

	return string(b), nil
}

func (post *Entry) MarkdownContent() (string, error) {
	buffer := new(bytes.Buffer)
	err := godown.Convert(buffer, strings.NewReader(post.Content), nil)
	if err != nil {
		return "", err
	}
	md := buffer.String()

	// Post-conversion
	// Do we need to trim anything?
	md = strings.TrimSpace(md)

	return md, nil
}

// Filename returns a suitable filename for the the entry
func (post *Entry) Filename() string {
	// TODO: make configurable
	return fmt.Sprintf("content/post/%s-%s.md",
		post.Date.Format("2006-01-02"),
		post.Slug())
}

// Text returns the text content, exlucing HTML markup
func (post *Entry) Text() string {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(post.Content))
	if err != nil {
		return post.Content
	}

	return doc.Find("body").Text()
}

// Slug returns a suitable slug for the entry
func (post *Entry) Slug() string {
	if post.Name != "" {
		return slug.Make(post.Name)
	}

	return trimFromN(slug.Make(post.Text()), "-", 3)
}

func trimFromN(s string, cutset string, n int) string {
	count := 0
	for pos, char := range s {
		if strings.ContainsRune(cutset, char) == false {
			continue
		}

		count++

		if count == n {
			return s[:pos]
		}
	}

	return s
}

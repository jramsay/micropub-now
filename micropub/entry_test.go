package micropub

import (
	"testing"
	"time"
)

func TestFilename(t *testing.T) {
	date, _ := time.Parse("2006-01-02 15:04", "2020-03-12 09:15")

	e := Entry{
		Name:    "Amazing post about something",
		Content: "This is a very very very very long article",
		Date:    date,
	}

	expectedFilename := "content/post/2020-03-12-amazing-post-about-something.md"
	if filename := e.Filename(); filename != expectedFilename {
		t.Errorf("unexpected filename: got %v want %v", filename, expectedFilename)
	}
}

func TestSlug_Name(t *testing.T) {
	e := Entry{
		Name:    "Amazing post about something",
		Content: "This is a very very very very long article",
	}

	expectedSlug := "amazing-post-about-something"
	if slug := e.Slug(); slug != expectedSlug {
		t.Errorf("unexpected slug: got %v want %v", slug, expectedSlug)
	}
}

func TestSlug_Content(t *testing.T) {
	e := Entry{
		Content: "This is a very very very very long article",
	}

	expectedSlug := "this-is-a"
	if slug := e.Slug(); slug != expectedSlug {
		t.Errorf("unexpected slug: got %v want %v", slug, expectedSlug)
	}
}

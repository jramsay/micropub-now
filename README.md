# micropub-now

✨ Micropub endpoints for Git based static-sites

Provides basic micropub endpoint for use with static sites hosted on GitLab. The
media upload endpoint depends on an AWS S3 bucket for temporary storage of media
uploads before a post is created.

## Configuration

The following environment variables should be set:

- `GITLAB_TOKEN` for authentication to GitLab.com
- `GITLAB_PROJECT_ID` the source project of the static site
- `GITLAB_PROJECT_BRANCH` the target branch
- `MICROPUB_AWS_ACCESS_KEY` for temporary media storage
- `MICROPUB_AWS_SECRET_ACCESS_TOKEN` for temporary media storage
- `MICROPUB_AWS_S3_BUCKET` for temporary media storage
- `ME_URL` for authentication

## Control flow

- Request with Indieauth token
- Verify the Indieauth token is valid
- With the authenticated domain
  - Retrieve post/media configuration from the environment
  - Retrieve post/media configuration from filesystem or S3 bucket
- Process request according to the domain configuration

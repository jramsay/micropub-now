package indieauth

import (
	"testing"
)

func TestCompareDomains(t *testing.T) {
	fixtureBasic := "https://example.com"
	if err := compareHostnames(fixtureBasic, fixtureBasic); err != nil {
		t.Errorf("unexpected mismatch: identical input")
	}

	fixtureInsecure := "http://example.com"
	if err := compareHostnames(fixtureBasic, fixtureInsecure); err != nil {
		t.Errorf("unexpected mismatch: schema should be ignored")
	}

	fixtureTld := "https://example.com.au"
	if err := compareHostnames(fixtureBasic, fixtureTld); err == nil {
		t.Errorf("unexpected match: different TLD")
	}

	fixtureSubdomain := "https://www.example.com"
	if err := compareHostnames(fixtureBasic, fixtureSubdomain); err == nil {
		t.Errorf("unexpected match: naked domain ownership doesn't include subdomains")
	}
}

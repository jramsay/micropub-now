package indieauth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type Client interface {
	VerifyAccessToken(bearerToken string) (TokenResponse, error)
	Middleware(next http.Handler) http.Handler
}

func NewClient(tokenEndpoint string) Client {
	return client{
		TokenEndpoint: tokenEndpoint,
	}
}

// TokenResponse implements IndieAuth Token Response
type TokenResponse struct {
	Me               string `json:"me"`
	ClientID         string `json:"client_id"`
	Scope            string `json:"scope"`
	IssuedBy         string `json:"issued_by"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
	StatusCode       int
}

type client struct {
	TokenEndpoint string
}

func (client client) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bearerToken := r.Header.Get("Authorization")
		tokenResponse, err := client.VerifyAccessToken(bearerToken)
		if err != nil {
			http.Error(w, "Forbidden", http.StatusUnauthorized)
			log.Println(err)
			return
		}

		u, err := url.Parse(tokenResponse.Me)
		if err != nil {
			return
		}
		hostname := strings.ToLower(u.Hostname())

		if err := compareHostnames(tokenResponse.Me, os.Getenv("ME_URL")); err != nil {
			http.Error(w, "Forbidden", http.StatusUnauthorized)
			log.Println(err)
			return
		}

		ctx := context.WithValue(r.Context(), "hostname", hostname)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (client client) VerifyAccessToken(bearerToken string) (TokenResponse, error) {
	if bearerToken == "" {
		log.Printf("no token")
		return TokenResponse{}, nil
	}

	req, err := http.NewRequest("GET", client.TokenEndpoint, nil)
	if err != nil {
		log.Printf("failed to create GET request: %s", err.Error())
		return TokenResponse{}, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", bearerToken)
	req.Header.Add("Accept", "application/json")

	c := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	resp, err := c.Do(req)
	if err != nil {
		log.Printf("failed to GET token endpoint: %s", err.Error())
		return TokenResponse{}, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed to read response body: %s", err.Error())
		return TokenResponse{}, err
	}

	tokenRes := TokenResponse{StatusCode: resp.StatusCode}
	err = json.Unmarshal(body, &tokenRes)
	if err != nil {
		log.Printf("failed to unmarshal response body: %s", err.Error())
		return TokenResponse{}, err
	}
	return tokenRes, nil
}

func compareHostnames(a string, b string) error {
	h1, err := normalizedHostname(a)
	if err != nil {
		return err
	}

	h2, err := normalizedHostname(b)
	if err != nil {
		return err
	}

	if h1 != h2 {
		return fmt.Errorf("hostnames do not match, %s is not %s", h1, h2)
	}

	return nil
}

func normalizedHostname(h string) (string, error) {
	u, err := url.Parse(h)
	if err != nil {
		return "", err
	}

	return strings.ToLower(u.Hostname()), nil
}

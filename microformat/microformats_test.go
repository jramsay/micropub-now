package microformat

import (
	"encoding/json"
	"log"
	"testing"
	"time"
)

func timeMustParse(format string, t string) time.Time {
	date, err := time.Parse(format, t)
	if err != nil {
		log.Fatalf("could not parse time %s with format %s", t, format)
	}

	return date
}

func TestTimeUnmarshalJSON(t *testing.T) {
	tests := map[string]struct {
		input  []byte
		output Time
	}{
		"iso": {
			input:  []byte(`["2020-03-01T17:20:58-07:00"]`),
			output: Time(timeMustParse("2006-01-02T15:04:05-07:00", "2020-03-01T17:20:58-07:00")),
		},
	}

	for testName, test := range tests {
		var output Time

		err := json.Unmarshal(test.input, &output)
		if err != nil {
			t.Errorf("%s: unexpected error: %v", testName, err)
		}

		if !time.Time(output).Equal(time.Time(test.output)) {
			t.Errorf("%s: unexpected output: got %v want %v", testName, output, test.output)
		}
	}
}

func TestStringUnmarshalJSON(t *testing.T) {
	tests := map[string]struct {
		input  []byte
		output String
	}{
		"plaintext": {
			input:  []byte(`["example"]`),
			output: String("example"),
		},
		"html": {
			input:  []byte(`[{"html": "<h1>Hello!</h1>"}]`),
			output: String("<h1>Hello!</h1>"),
		},
	}

	for testName, test := range tests {
		var output String

		err := json.Unmarshal(test.input, &output)
		if err != nil {
			t.Errorf("%s: unexpected error: %v", testName, err)
		}

		if output != test.output {
			t.Errorf("%s: unexpected output: got %s want %s", testName, output, test.output)
		}
	}
}

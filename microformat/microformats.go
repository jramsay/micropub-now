package microformat

import (
	"encoding/json"
	"fmt"
	"log"
	"time"
)

type String string
type Time time.Time

type micropubHTMLContent struct {
	HTML string `json:"html"`
}

func (s *String) UnmarshalJSON(b []byte) error {
	// String: "key": ["value"]
	var ss []string
	if err := json.Unmarshal(b, &ss); err == nil {
		*s = String(ss[0])
		return nil
	}

	// Object: "key": [{"html": "value"}]
	var content []micropubHTMLContent
	if err := json.Unmarshal(b, &content); err == nil {
		*s = String(content[0].HTML)
		return nil
	}

	return fmt.Errorf("unrecognized Microformat2 String: %s", b)
}

func (d *Time) UnmarshalJSON(b []byte) error {
	var ss []string
	if err := json.Unmarshal(b, &ss); err != nil {
		return err
	}

	*d = Time(normalizeDate(ss[0]))
	return nil
}

func normalizeDate(d string) time.Time {
	// TODO: add Epoch support
	// Share with API implemetation
	formats := []string{
		// T time sep
		"2006-01-02T15:04:05Z07:00",
		"2006-01-02T15:04:05Z0700",
		"2006-01-02T15:04:05Z07",
		"2006-01-02T15:04:05",

		// space time sep
		"2006-01-02 15:04:05Z07:00",
		"2006-01-02 15:04:05Z0700",
		"2006-01-02 15:04:05Z07",
		"2006-01-02 15:04:05",

		// no seconds T time sep
		"2006-01-02T15:04Z07:00",
		"2006-01-02T15:04Z0700",
		"2006-01-02T15:04Z07",

		// no seconds space time sep
		"2006-01-02 15:04Z07:00",
		"2006-01-02 15:04Z0700",
		"2006-01-02 15:04Z07",

		"2006-01-02",
		"2006-01",
		"2006",
	}

	for _, format := range formats {
		if t, err := time.Parse(format, d); err == nil {
			return t
		}
	}

	log.Printf("could not parse date: %v", d)
	return time.Now()
}

# Documentation

## Configuration (RFC)

In the root of the target repo:

```toml
[paths]
  post = "/content/post/:year-:month-:day-:slug"
  media = "/content/uploads/:year/:slug"
```

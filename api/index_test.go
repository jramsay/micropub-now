package api

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/jramsay/micropub-now/micropub"
	"gitlab.com/jramsay/micropub-now/storage"
)

type MockStorage struct {
	t              *testing.T
	name           string
	expectedOutput string
}

func (m MockStorage) Publish(e *micropub.Entry, files []*storage.File) error {
	if e.String() != m.expectedOutput {
		m.t.Errorf("unexpected output: got %s want %s", e.String(), m.expectedOutput)
	}

	return nil
}

func TestPublishHandlers(t *testing.T) {
	tests := map[string]struct {
		rawQuery       string
		body           string
		contentType    string
		expectedOutput string
		err            error
	}{
		"form": {
			rawQuery:       "h=entry&content=Greetings%20from%20Australia%21&date=2020-03-01T17%3A20%3A58-07%3A00",
			contentType:    "application/x-www-form-urlencoded",
			expectedOutput: "---\n\nslug: greetings-from-australia\ndate: 2020-03-01T17:20:58-07:00\n\n---\n\nGreetings from Australia!\n",
		},
		"json": {
			body:           `{"type":["h-entry"],"properties":{"name":[""],"content":[{"html":"<h1>Hello!</h1>"}],"photo":[],"date":["2020-03-01T17:20:58-07:00"]}}`,
			contentType:    "application/json",
			expectedOutput: "---\n\nslug: hello\ndate: 2020-03-01T17:20:58-07:00\n\n---\n\n# Hello!\n",
		},
	}

	for testName, test := range tests {
		siteStorage = MockStorage{t, testName, test.expectedOutput}
		var body = []byte(test.body)

		req, err := http.NewRequest("POST", "/api/micropub", bytes.NewBuffer(body))
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Set("Content-Type", test.contentType)
		req.URL.RawQuery = test.rawQuery

		r := httptest.NewRecorder()
		handler := http.HandlerFunc(authenticatedHandler)

		handler.ServeHTTP(r, req)

		expectedStatus := http.StatusAccepted
		if status := r.Code; status != expectedStatus {
			t.Errorf("%s: handler returned wrong status code: got %v want %v",
				testName, status, expectedStatus)
		}

		// if location := r.Result().Header.Get("Location"); location == "" {
		// t.Errorf("location missing")
		// }
	}
}

func TestPermalink(t *testing.T) {
	date, _ := time.Parse("2006-01-02 15:04", "2020-03-12 09:15")
	entry := &micropub.Entry{
		Content: "This morning I went for a walk in the sunshine.",
		Name:    "",
		Date:    date,
	}

	permalink, err := expandPermalink(entry, "/:year/:month/:day/:slug/")
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	expectedOutput := "/2020/03/12/this-morning-i/"
	if permalink != expectedOutput {
		t.Errorf("unexpected permalink: got %s want %s", permalink, expectedOutput)
	}
}

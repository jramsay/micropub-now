package api

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jramsay/micropub-now/indieauth"
	"gitlab.com/jramsay/micropub-now/micropub"
	"gitlab.com/jramsay/micropub-now/site"
	"gitlab.com/jramsay/micropub-now/storage"
)

var siteStorage storage.SiteStorage = storage.NewGitLabStorage(
	os.Getenv("GITLAB_TOKEN"),
	os.Getenv("GITLAB_PROJECT_ID"),
	os.Getenv("GITLAB_PROJECT_BRANCH"))

var mediaStorage storage.MediaStorage = storage.NewS3MediaStorage(
	os.Getenv("MICROPUB_AWS_ACCESS_KEY"),
	os.Getenv("MICROPUB_AWS_SECRET_ACCESS_TOKEN"),
	os.Getenv("MICROPUB_AWS_S3_BUCKET"))

// Handler for Micropub POST requests
func Handler(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling url: ", r.URL.Path)

	router := http.NewServeMux()
	router.HandleFunc("/", authenticatedHandler)

	auth := indieauth.NewClient(os.Getenv("INDIEAUTH_TOKEN_ENDPOINT"))
	authenticatedRouter := auth.Middleware(router)

	authenticatedRouter.ServeHTTP(w, r)
}

func authenticatedHandler(w http.ResponseWriter, r *http.Request) {
	router := http.NewServeMux()

	switch r.Method {
	case http.MethodGet:
		router.HandleFunc("/api/micropub", configHandler)
	case http.MethodPost:
		router.HandleFunc("/api/micropub/media", mediaUploadHandler)

		// Workaround for Sunlit not respecting the Media Endpoint
		pattern, _ := regexp.Compile("multipart/form-data(;.*)?")
		if pattern.MatchString(r.Header.Get("Content-Type")) {
			router.HandleFunc("/api/micropub", mediaUploadHandler)
		}

		router.HandleFunc("/api/micropub", publishHandler)
	case http.MethodPut:
		// Update an existing record.
	case http.MethodDelete:
		// Remove the record.
	default:
		// Give an error message.
	}

	router.ServeHTTP(w, r)
}

// configHandler implements the Micropub configuration discovery endpoint
// https://www.w3.org/TR/micropub/#configuration
func configHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("GET micropub?config - %s", r.UserAgent())
	scheme := "https"
	w.Header().Set("Content-Type", "application/json")
	resp := fmt.Sprintf("{\n  \"media-endpoint\": \"%s://%s/api/micropub/media\"\n}\n", scheme, r.Host)
	log.Println(resp)
	fmt.Fprintf(w, "%s", resp)
}

// mediaUploadHandler implements the Micropub media upload endpoint
// https://www.w3.org/TR/micropub/#media-endpoint
func mediaUploadHandler(w http.ResponseWriter, r *http.Request) {
	maxSize := int64(5 << 20) // 5 MB - set by Vercel
	r.Body = http.MaxBytesReader(w, r.Body, maxSize)

	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer file.Close()

	filename, err := mediaStorage.Put(file, fileHeader)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Location", filename)
	w.WriteHeader(http.StatusCreated)
}

// publishHandler implements the Micropub post creation endpoint
// https://www.w3.org/TR/micropub/#create
func publishHandler(w http.ResponseWriter, r *http.Request) {
	entry, err := micropub.Unmarshal(r)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if entry == nil {
		log.Println("entry should not be nil")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	permalink, err := publishEntry(entry)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Location", permalink)
	w.WriteHeader(http.StatusAccepted)
}

func publishEntry(e *micropub.Entry) (string, error) {
	if e.Date.IsZero() {
		e.Date = time.Now()
	}

	s := site.Site{
		ContentPath:      "/content/:year:month:day-:slug.md",
		ContentPermalink: "/:year/:month/:day/:slug",
		UploadPath:       "/content/uploads/:year/:shortid.:ext",
		UploadPermalink:  "/uploads/:year/:shortid.:ext",
	}

	permalink, files, err := s.Post(e)
	if err != nil {
		return "", err
	}

	if err := siteStorage.Publish(e, files); err != nil {
		return "", err
	}

	u, _ := url.Parse(os.Getenv("ME_URL"))
	u.Path = path.Join(u.Path, permalink)

	return u.String(), nil
}

// Example: /:year/:month/:day/:slug/
func expandPermalink(e *micropub.Entry, pattern string) (string, error) {
	if !strings.HasPrefix(pattern, "/") {
		return "", errors.New("Invalid permalink pattern. Expected prefix '/'")
	}

	fragments := strings.Split(pattern[1:], "/")
	for i, part := range fragments {
		if !strings.HasPrefix(part, ":") {
			continue
		}

		replacement, err := expandPermalinkPart(e, part[1:])
		if err != nil {
			return "", err
		}

		fragments[i] = replacement
	}

	return "/" + strings.Join(fragments, "/"), nil
}

func expandPermalinkPart(e *micropub.Entry, field string) (string, error) {
	switch field {
	case "year":
		return strconv.Itoa(e.Date.Year()), nil
	case "month":
		return fmt.Sprintf("%02d", int(e.Date.Month())), nil
	case "day":
		return fmt.Sprintf("%02d", e.Date.Day()), nil
	case "slug":
		return e.Slug(), nil
	}

	return "", errors.New("Invalid permalink field.")
}

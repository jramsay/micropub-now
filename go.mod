module gitlab.com/jramsay/micropub-now

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/aws/aws-sdk-go v1.29.8
	github.com/go-test/deep v1.0.5
	github.com/google/go-cmp v0.5.4
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
	github.com/gosimple/slug v1.9.0
	github.com/mattn/godown v0.0.0-20200217152941-afc959f6a561
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	github.com/xanzy/go-gitlab v0.26.0
	gopkg.in/yaml.v3 v3.0.0-20200121175148-a6ecf24a6d71
	moul.io/http2curl v1.0.0
)

package site

import (
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/jramsay/micropub-now/micropub"
	"gitlab.com/jramsay/micropub-now/storage"
)

func TestPost(t *testing.T) {
	realShortID := shortID
	defer func() { shortID = realShortID }()
	shortID = func() string { return "UfEP4BfMg" }

	site := Site{
		ContentPath:      "/content/:year:month:day-:slug.md",
		ContentPermalink: "/:year/:month/:day/:slug",
		UploadPath:       "/content/uploads/:year/:shortid.:ext",
		UploadPermalink:  "/uploads/:year/:shortid.:ext",
	}

	date, _ := time.Parse("2006-01-02 15:04", "2020-03-12 09:15")
	entry := micropub.Entry{
		Date:    date,
		Content: "Spicy hot take",
		Media: []*micropub.MediaFile{
			{
				URL:          "https://www.fillmurray.com/400/400",
				Data:         []byte("Fill Murray"),
				LastModified: date,
			},
		},
	}

	expectedContent := `---
slug: spicy-hot-take
date: 2020-03-12 09:15:00 +0000 UTC
photos:
- /content/uploads/2020/UfEP4BfMg.jpg
---

Spicy hot take
`

	expectedOutput := []*storage.File{
		{
			Path:          "/content/uploads/2020/UfEP4BfMg.jpg",
			BinaryContent: []byte("Fill Murray"),
		},
		{
			Path:        "/content/20200312-spicy-hot-take.md",
			TextContent: expectedContent,
		},
	}

	_, output, err := site.Post(&entry)
	if err != nil {
		t.Errorf("expected no errors, got: %v", err)
	}

	if diff := cmp.Diff(output, expectedOutput); diff != "" {
		t.Error(diff)
	}
}

func TestExpandPermalink(t *testing.T) {
	table := []struct {
		pattern  string
		expected string
	}{
		{
			pattern:  "/content/:year:month:day-:slug.md",
			expected: "/content/20200312-jaberwocky.md",
		}, {
			pattern:  "/:year/:month/:day/:slug",
			expected: "/2020/03/12/jaberwocky",
		}, {
			pattern:  "/content/uploads/:year/:shortid.:ext",
			expected: "/content/uploads/2020/3TKR3JfMgz.jpg",
		}, {
			pattern:  "/uploads/:year/:shortid.:ext",
			expected: "/uploads/2020/3TKR3JfMgz.jpg",
		},
	}

	date, _ := time.Parse("2006-01-02 15:04", "2020-03-12 09:15")
	fields := PermalinkFields{
		Date:    date,
		Slug:    "jaberwocky",
		ShortID: "3TKR3JfMgz",
		File: FileVariables{
			Ext: "jpg",
		},
	}

	for _, test := range table {
		output, err := expandPermalink(test.pattern, fields)
		if err != nil {
			t.Errorf("expected no errors, got: %v", err)
		}

		if output != test.expected {
			t.Errorf("got '%v', want '%v'", output, test.expected)
		}
	}
}

package site

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/teris-io/shortid"

	"gitlab.com/jramsay/micropub-now/micropub"
	"gitlab.com/jramsay/micropub-now/storage"
)

type Site struct {
	ContentPath      string
	ContentPermalink string
	UploadPath       string
	UploadPermalink  string
}

// FileVariables made available during rendering
type FileVariables struct {
	Path         string
	Ext          string
	BaseFileName string
}

// Post is passed to the post template for rendering
type Post struct {
	Title      string
	Slug       string
	Date       time.Time
	Categories []string
	Photos     []Photo
	Content    string
	// Output information
	Permalink string
	File      FileVariables
}

type Photo struct {
	Title string
	// Output inforrmation
	Permalink string
	File      FileVariables
}

type PermalinkFields struct {
	Date    time.Time
	Slug    string
	ShortID string
	File    FileVariables
}

func (s *Site) Post(e *micropub.Entry) (string, []*storage.File, error) {
	var files []*storage.File
	var photos []Photo

	for _, photo := range e.Media {
		v := photoPermalinkFields(photo)
		filepath, err := expandPermalink(s.UploadPath, v)
		if err != nil {
			return "", nil, err
		}

		permalink, err := expandPermalink(s.UploadPermalink, v)
		if err != nil {
			return "", nil, err
		}

		f := storage.File{
			Path:          filepath,
			BinaryContent: photo.Data,
		}
		files = append(files, &f)

		p := Photo{
			Permalink: permalink,
			File: FileVariables{
				Path: filepath,
			},
		}
		photos = append(photos, p)
	}

	// Content
	v := postPermalinkFields(e)
	filepath, err := expandPermalink(s.ContentPath, v)
	if err != nil {
		return "", nil, err
	}

	permalink, err := expandPermalink(s.ContentPermalink, v)
	if err != nil {
		return "", nil, err
	}

	p := Post{
		Content:   e.Content,
		Slug:      e.Slug(),
		Date:      e.Date,
		Permalink: permalink,
		Photos:    photos,
		File: FileVariables{
			Path: filepath,
		},
	}

	// Create a new template and parse the letter into it.
	t := template.Must(template.New("post").Parse(PostTemplate))

	var content bytes.Buffer
	if err := t.Execute(&content, p); err != nil {
		return "", nil, err
	}

	f := storage.File{
		Path:        filepath,
		TextContent: prettify(content.String()),
	}
	files = append(files, &f)

	return "", files, nil
}

func prettify(input string) string {
	var output []string

	lines := strings.Split(input, "\n\n")
	for _, line := range lines {
		if line != "" {
			output = append(output, strings.TrimLeft(line, "\n"))
		}
	}

	return strings.Join(output, "\n\n")
}

func postPermalinkFields(e *micropub.Entry) PermalinkFields {
	return PermalinkFields{
		Date: e.Date,
		Slug: e.Slug(),
		File: FileVariables{
			Ext: "md",
		}}
}

func photoPermalinkFields(m *micropub.MediaFile) PermalinkFields {
	return PermalinkFields{
		Date:    m.LastModified,
		ShortID: shortID(),
		File: FileVariables{
			Ext: "jpg",
		}}
}

// Example: /:year/:month/:day/:slug/
func expandPermalink(pattern string, values PermalinkFields) (string, error) {
	if !strings.HasPrefix(pattern, "/") {
		return "", errors.New("Invalid permalink pattern. Expected prefix '/'")
	}

	fragments := strings.Split(pattern[1:], "/")
	for i, fragment := range fragments {
		units := strings.Split(fragment, ":")
		for j, unit := range units {
			if j == 0 && !strings.HasPrefix(fragment, ":") {
				continue
			}

			replacement, remainder, err := expandPermalinkPart(unit, values)
			if err != nil {
				return "", err
			}

			units[j] = replacement + remainder
		}
		fragments[i] = strings.Join(units, "")
	}

	return "/" + strings.Join(fragments, "/"), nil
}

func expandPermalinkPart(field string, v PermalinkFields) (string, string, error) {
	switch {
	case field == "":
		return "", "", nil
	case strings.HasPrefix(field, "year"):
		return strconv.Itoa(v.Date.Year()), strings.TrimPrefix(field, "year"), nil
	case strings.HasPrefix(field, "month"):
		return fmt.Sprintf("%02d", int(v.Date.Month())), strings.TrimPrefix(field, "month"), nil
	case strings.HasPrefix(field, "day"):
		return fmt.Sprintf("%02d", v.Date.Day()), strings.TrimPrefix(field, "day"), nil
	case strings.HasPrefix(field, "slug"):
		return v.Slug, strings.TrimPrefix(field, "slug"), nil
	case strings.HasPrefix(field, "shortid"):
		return v.ShortID, strings.TrimPrefix(field, "shortid"), nil
	case strings.HasPrefix(field, "ext"):
		return v.File.Ext, strings.TrimPrefix(field, "ext"), nil
	}

	return "", field, errors.New("Invalid permalink field.")
}

var shortID = func() string {
	return shortid.MustGenerate()
}

package site

const PostTemplate = `---
{{- with .Title }}
title: {{ . }}{{ end }}
{{- with .Slug }}
slug: {{ . }}{{ end }}
{{- with .Date }}
date: {{ . }}{{ end }}
{{- with .Categories }}
categories: {{ range . }}
- {{ . }}{{ end }}{{ end }}
{{- with .Photos }}
photos:{{ range . }}
- {{ .File.Path }}{{ end }}{{ end }}
---

{{ with .Content }}{{ . }}{{ end }}
`

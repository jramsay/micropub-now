# Roadmap

- 0.1.0: Create posts from Micro.blog and Sunlit apps
  - Detect HTML and markdown content and name output appropriately
  - Set photo type frontmatter
- 0.2.0: Rewrite HTML to markdown

### TODO

- calculate media storage paths (target media)
- target entry path and media path should be calculated by the Site, not storage
- entry toString needs to know the media destination paths

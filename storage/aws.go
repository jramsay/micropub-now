package storage

import (
	"log"
	"mime/multipart"
	"path/filepath"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/teris-io/shortid"
)

// S3MediaStorage implements a temporary media storage location in Amazon S3.
type S3MediaStorage struct {
	session *session.Session
	bucket  string
}

// NewS3MediaStorage creates a new media storage location.
func NewS3MediaStorage(id string, secret string, bucket string) S3MediaStorage {
	s, err := session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(id, secret, ""),
	})
	if err != nil {
		log.Println(err)
		return S3MediaStorage{}
	}

	return S3MediaStorage{
		session: s,
		bucket:  bucket,
	}
}

// Put the uploaded media in a temporary storage location, and return a
// temporary link.
func (s S3MediaStorage) Put(file multipart.File, fileHeader *multipart.FileHeader) (string, error) {
	key := "" + shortid.MustGenerate() + filepath.Ext(fileHeader.Filename)
	expiry := time.Now().Add(1 * time.Hour)

	uploader := s3manager.NewUploader(s.session)

	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket:      aws.String(s.bucket),
		ContentType: aws.String(fileHeader.Header.Get("Content-Type")),
		Expires:     &expiry,
		Key:         aws.String(key),
		Body:        file,
	})
	if err != nil {
		return "", err
	}

	// TODO: returned presigned URL
	return result.Location, err
}

func (s S3MediaStorage) isMediaLink(url string) bool {
	if strings.HasPrefix(url, "https://"+s.bucket+".") {
		return true
	}

	return false
}

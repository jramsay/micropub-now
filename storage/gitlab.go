package storage

import (
	"encoding/base64"
	"fmt"
	"log"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/micropub-now/micropub"
)

type GitLabStorage struct {
	projectID string
	branch    string
	token     string
}

func NewGitLabStorage(token string, projectID string, branch string) *GitLabStorage {
	return &GitLabStorage{
		projectID: projectID,
		branch:    branch,
		token:     token,
	}
}

func (g *GitLabStorage) Publish(post *micropub.Entry, files []*File) error {
	commitMessage := fmt.Sprintf("Post: %s", post.Date.Format("2006-01-02-150405"))

	git := gitlab.NewClient(nil, g.token)
	c := &gitlab.CreateCommitOptions{
		Branch:        &g.branch,
		CommitMessage: &commitMessage,
		Actions:       []*gitlab.CommitAction{},
	}

	for _, f := range files {
		c.Actions = append(c.Actions, &gitlab.CommitAction{
			Action:   "create",
			FilePath: f.Path,
			Content:  f.Content(),
			Encoding: f.Encoding(),
		})
	}

	commit, _, err := git.Commits.CreateCommit(g.projectID, c, nil)
	if err != nil {
		return err
	}

	log.Printf("created commit %s", commit.ID)
	return nil
}

func (f *File) Encoding() string {
	if f.BinaryContent != nil {
		return "base64"
	}

	return "text"
}

func (f *File) Content() string {
	if f.Encoding() == "base64" {
		return base64.StdEncoding.EncodeToString(f.BinaryContent)
	}

	return f.TextContent
}

package storage

import (
	"mime/multipart"

	"gitlab.com/jramsay/micropub-now/micropub"
)

// File containing Post or Media content that will be writen to disk:w
type File struct {
	Path          string
	TextContent   string
	BinaryContent []byte
}

// SiteStorage interface for site storage like GitLab, GitHub, or other Git forges
type SiteStorage interface {
	Publish(entry *micropub.Entry, files []*File) error
}

// MediaStorage interface for media endpoint storage like AWS S3, or some other object storage
type MediaStorage interface {
	Put(file multipart.File, fileHeader *multipart.FileHeader) (string, error)
}
